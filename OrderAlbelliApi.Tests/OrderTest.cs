﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OrderAlbelliApi.Controllers;
using OrderAlbelliApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderAlbelliApi.Tests
{
    [TestClass]
    public class OrderTest
    {
        OrderController controller;
        [TestMethod]
        public void Get()
        {
            // Arrange
            controller = new OrderController();

            // Act
            var result = controller.Get();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Count());
        }

        [TestMethod]
        public void GetById()
        {
            // Arrange
            controller = new OrderController();

            // Act
            dynamic response = controller.Get(1);  //Check for order 1

            // Assert
            Assert.IsNotNull(response.Content);
            Assert.AreEqual("The total bin width for order Id 1 is 317mm", response.Content.MinimumRequiredBinWidth);
        }

        [TestMethod]
        public void Post()
        {
            // Arrange
            //ValuesController controller = new ValuesController();
            OrderController controller = new OrderController();
            //controller.Request = new HttpRequestMessage();
            //controller.Configuration = new HttpConfiguration();

            List<OrderDetail> _odObject = new List<OrderDetail>
            {
                new OrderDetail {ProductId = 5,Quantity = 5 },
                new OrderDetail {ProductId = 1,Quantity = 1 },
                new OrderDetail {ProductId = 3,Quantity = 2 }
            };
            // Act 
            //var resultSet = controller.Post(_odObject) as CreatedNegotiatedContentResult<OrderDetail>;
            dynamic resultSet = controller.Post(_odObject);
            // Assert  
            Assert.IsNotNull(resultSet);
            Assert.AreEqual("Order saved successfully. The total bin width for order Id 5 is 227mm", resultSet.Content);
        }
    }
}
