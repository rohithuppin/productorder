﻿using OrderAlbelliApi.CustomFilters;
using OrderAlbelliApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OrderAlbelliApi.Controllers
{
    [CustomExceptionHandlerFilter]
    public class OrderController : ApiController
    {
        #region Local storage to run the application without using database
        static List<Order> _orderGlobalObject = new List<Order>() {
                new Order { OrderId = 1, OrderDate = DateTime.Now.AddMonths(-2), CreatedBy="Admin", CreatedDate = Convert.ToDateTime(DateTime.Now.ToString("dddd, dd MMMM yyyy")), IsActive =true },
                new Order { OrderId = 2, OrderDate = DateTime.Now.AddMonths(-1), CreatedBy="Admin", CreatedDate= Convert.ToDateTime(DateTime.Now.ToString("dddd, dd MMMM yyyy")), IsActive =true },
                new Order { OrderId = 3, OrderDate = DateTime.Now.AddHours(-5), CreatedBy="Admin", CreatedDate= Convert.ToDateTime(DateTime.Now.ToString("dddd, dd MMMM yyyy")), IsActive =true },
                new Order { OrderId = 4, OrderDate = DateTime.Now.AddMinutes(-15), CreatedBy="Admin", CreatedDate= Convert.ToDateTime(DateTime.Now.ToString("dddd, dd MMMM yyyy")), IsActive =true }
            };
        static List<OrderDetail> _orderDetailsGlobalObject = new List<OrderDetail>() {
                new OrderDetail { OrderDetailID = 1, OrderId = 1, ProductId = 1, Quantity = 1,CreatedBy="Admin", CreatedDate= Convert.ToDateTime(DateTime.Now.ToString("dddd, dd MMMM yyyy")), IsActive =true },
                new OrderDetail { OrderDetailID = 2, OrderId = 1, ProductId = 2, Quantity = 1,CreatedBy="Admin", CreatedDate= Convert.ToDateTime(DateTime.Now.ToString("dddd, dd MMMM yyyy")), IsActive =true },
                new OrderDetail { OrderDetailID = 3, OrderId = 1, ProductId = 5, Quantity = 9,CreatedBy="Admin", CreatedDate= Convert.ToDateTime(DateTime.Now.ToString("dddd, dd MMMM yyyy")), IsActive =true },
                new OrderDetail { OrderDetailID = 4, OrderId = 2, ProductId = 2, Quantity = 2,CreatedBy="Admin", CreatedDate= Convert.ToDateTime(DateTime.Now.ToString("dddd, dd MMMM yyyy")), IsActive =true },
                new OrderDetail { OrderDetailID = 5, OrderId = 2, ProductId = 5, Quantity = 4,CreatedBy="Admin", CreatedDate= Convert.ToDateTime(DateTime.Now.ToString("dddd, dd MMMM yyyy")), IsActive =true },
                new OrderDetail { OrderDetailID = 6, OrderId = 4, ProductId = 3, Quantity = 3,CreatedBy="Admin", CreatedDate= Convert.ToDateTime(DateTime.Now.ToString("dddd, dd MMMM yyyy")), IsActive =true },
                new OrderDetail { OrderDetailID = 7, OrderId = 4, ProductId = 5, Quantity = 1, CreatedBy="Admin", CreatedDate= Convert.ToDateTime(DateTime.Now.ToString("dddd, dd MMMM yyyy")), IsActive =true }
            };
        static List<Product> _productsGlobalObject = new List<Product>() {
                   new Product { ProductTypeID=1, ProductName ="PhotoBook", ProductWidth = 19 },
                   new Product { ProductTypeID=2, ProductName ="canvaas", ProductWidth = 16 },
                   new Product { ProductTypeID=3, ProductName ="Calender", ProductWidth = 10},
                   new Product { ProductTypeID=4, ProductName ="Cards", ProductWidth = 4.7 },
                   new Product { ProductTypeID=5, ProductName ="Mugs", ProductWidth = 94 },
            };
        #endregion

        #region GET api/values - HTTP Get method to get all order details
        /// <summary>
        /// Get method to retrive all orders in the application
        /// </summary>
        /// <returns>List of orders</returns>
        public IEnumerable<Order> Get()
        {
            return _orderGlobalObject.AsEnumerable();
        }
        #endregion

        #region GET api/values/5 - HTTP Get method by Id returns given order details
        /// <summary>
        /// Get order details by passing order Id to the application
        /// </summary>
        /// <param name="id">Order Id</param>
        /// <returns>Returns order details and minimum required bin width for the given Order id</returns>
        public IHttpActionResult Get(int id)
        {
            var OrderDetailsList = from o in _orderGlobalObject
                                   join od in _orderDetailsGlobalObject on o.OrderId equals od.OrderId
                                   join p in _productsGlobalObject on od.ProductId equals p.ProductTypeID
                                   where od.OrderId == id
                                   orderby od.OrderDetailID
                                   select new
                                   {
                                       OrderId = od.OrderId,
                                       OrderDate = o.OrderDate.ToString("dddd, dd MMMM yyyy"),
                                       ProductType = p.ProductName,
                                       ProductWidth = p.ProductWidth,
                                       Quantity = od.Quantity
                                   };

            if (OrderDetailsList != null && OrderDetailsList.Count() > 0)
            {
                string MinimumRequiredBinWidth = GetTotalBinWidthofOrder(id);
                var resultSet = new { MinimumRequiredBinWidth, OrderDetailsList };
                return Ok(resultSet);
            }
            else
            {
                return Content(HttpStatusCode.NotFound, "Order with Id " + id.ToString() + " not found");
            }
        }
        #endregion

        #region Calculate total required width of the order
        /// <summary>
        /// This method returns minimun bin width required for the order
        /// </summary>
        /// <param name="orderid">order Id</param>
        /// <returns>Total Bin Width in mm</returns>
        private string GetTotalBinWidthofOrder(int orderid)
        {
            double _totalBinWidth = 0;
            int _mugContainerMaxQuantity;

            List<OrderDetail> _orderDetailsWidth = _orderDetailsGlobalObject.Where(p => p.OrderId == orderid).ToList(); //Gets matching order details from order details list
            for (int i = 0; i < _orderDetailsWidth.Count(); i++)
            {
                if (_orderDetailsWidth[i].ProductId == 5)   //check if the product is Mugs
                {
                    _mugContainerMaxQuantity = _orderDetailsWidth[i].Quantity / 4; //divide by 4 to know how many containers required for mugs

                    if (_orderDetailsWidth[i].Quantity % 4 > 0) //Mugs can be stacked upto 4, so check mod of product quantity is greater than zero.
                        _totalBinWidth += ((_productsGlobalObject[_orderDetailsWidth[i].ProductId - 1].ProductWidth) * _mugContainerMaxQuantity) + _productsGlobalObject[_orderDetailsWidth[i].ProductId - 1].ProductWidth;  //if quantity mod of 4 is greater than add one extra container
                    else
                        _totalBinWidth += (_productsGlobalObject[_orderDetailsWidth[i].ProductId - 1].ProductWidth) * _mugContainerMaxQuantity;
                }
                else     // multiply product width with quantity excluding Mugs
                    _totalBinWidth += (_productsGlobalObject[_orderDetailsWidth[i].ProductId - 1].ProductWidth) * _orderDetailsWidth[i].Quantity;
            }
            return "The total bin width for order Id " + orderid + " is " + _totalBinWidth + "mm";
        }
        #endregion

        #region POST api/values - HTTP Post method to post order details to backend
        /// <summary>
        /// Post method to save the order details
        /// </summary>
        /// <param name="orderDetailsList">List products and their quantity</param>
        /// <returns>Returns newly created order details</returns>
        /// 
        public IHttpActionResult Post([FromBody]IEnumerable<OrderDetail> orderDetailsList)
        {
            Order _orderCart = new Order();
            _orderCart.OrderId = _orderGlobalObject.Max(o => o.OrderId) + 1;
            _orderCart.OrderDate = DateTime.Now;

            OrderDetail _orderDetailsCart = null;
            foreach (var item in orderDetailsList)
            {
                _orderDetailsCart = new OrderDetail
                {
                    OrderDetailID = _orderDetailsGlobalObject.Max(od => od.OrderDetailID) + 1,
                    OrderId = _orderCart.OrderId,
                    ProductId = item.ProductId,
                    Quantity = item.Quantity,
                    CreatedBy = "PostVerb",
                    CreatedDate = DateTime.Now,
                    IsActive = true
                };
                if (_orderDetailsCart.ProductId < 1 || _orderDetailsCart.ProductId > 6)
                {
                    return BadRequest("Please enter a valid product");
                }
                if (_orderDetailsCart.Quantity <= 0)
                {
                    return BadRequest("Product " + _productsGlobalObject[_orderDetailsCart.ProductId - 1].ProductName + " quantity should be greater than zero");
                }
                _orderDetailsGlobalObject.Add(_orderDetailsCart);
            }

            _orderGlobalObject.Add(_orderCart);

            if (_orderDetailsCart == null)
            {
                return BadRequest("Error while ordering product. Please try with valid order details");
            }
            //return Created(Request.RequestUri + _orderCart._id.ToString(), _orderDetailsGlobalObject.Where(o => o._orderId == _orderCart._id).ToList());
            return Ok("Order saved successfully. " + GetTotalBinWidthofOrder(_orderCart.OrderId));
        }
        #endregion
    }
}
