﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace OrderAlbelliApi.CustomFilters
{
    public class CustomExceptionHandlerFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            var exception = context.Exception;
            if (exception != null)
            {
                if (exception is NotImplementedException)
                    context.Response = new HttpResponseMessage(HttpStatusCode.NotImplemented);
                else if (exception is ArgumentException)
                {
                    var message = exception.Message.Split(new[] { "\r\n" }, StringSplitOptions.None);
                    ExceptionHandling(HttpStatusCode.BadRequest, message[0]);
                }
                else if (exception is ApplicationException)
                    ExceptionHandling(HttpStatusCode.InternalServerError, exception.Message);
                else
                {
                    ExceptionHandling(HttpStatusCode.InternalServerError, @"Internal Server Error");
                }
            }
            base.OnException(context);
        }

        public void ExceptionHandling(HttpStatusCode statusCode, string message)
        {

            if (statusCode == HttpStatusCode.InternalServerError)
            {
                message = "Internal Server Error. Unable to call Albelli Api product order system.";
            }
            var errorResponse = new GenericErrorResponse
            {
                Message = message,
                StatusCode = ((int)statusCode).ToString()

            };
            var resp = new HttpResponseMessage(statusCode)
            {
                Content = new ObjectContent<GenericErrorResponse>(errorResponse, new JsonMediaTypeFormatter(), "application/json")
            };

            throw new HttpResponseException(resp);
        }
    }
}