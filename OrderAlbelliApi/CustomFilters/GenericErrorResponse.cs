﻿namespace OrderAlbelliApi.CustomFilters
{
    internal class GenericErrorResponse
    {
        public string Message { get; set; }
        public string StatusCode { get; set; }
    }
}