﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderAlbelliApi.Models
{
    /// <summary>
    /// Order class
    /// </summary>
    public class Order
    {
        /// <summary>
        /// Order Id to identify the each orders
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Ordered date
        /// </summary>
        public DateTime OrderDate { get; set; }

        /// <summary>
        /// Indicates If The Order Is Active or InActive
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// 
        /// Order Created Date
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Order Created By
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Order Modified Date
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Order Modified By
        /// </summary>
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Order Deleted Date
        /// </summary>
        public DateTime DeletedDate { get; set; }

        /// <summary>
        /// Order Deleted By
        /// </summary>
        public string DeletedBy { get; set; }

        /// <summary>
        /// Indicates If Order Is Deleted or Not
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}