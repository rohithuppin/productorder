﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderAlbelliApi.Models
{
    /// <summary>
    /// Product Entity 
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Product Id identifies each product
        /// </summary>
        public int ProductTypeID { get; set; }

        /// <summary>
        /// Name of the prodcut
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Width of the each product
        /// </summary>
        public double ProductWidth { get; set; }

        /// <summary>
        /// Indicates If The Order Is Active or InActive
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// 
        /// Order Created Date
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Order Created By
        /// </summary>
        public DateTime CreatedBy { get; set; }

        /// <summary>
        /// Order Modified Date
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Order Modified By
        /// </summary>
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Order Deleted Date
        /// </summary>
        public DateTime DeletedDate { get; set; }

        /// <summary>
        /// Order Deleted By
        /// </summary>
        public string DeletedBy { get; set; }

        /// <summary>
        /// Indicates If Order Is Deleted or Not
        /// </summary>
        public bool IsDeleted { get; set; }
    }

}